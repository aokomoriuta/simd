#define _SCL_SECURE_NO_WARNINGS
#define __CL_ENABLE_EXCEPTIONS

#pragma warning(push, 1)
#pragma warning(disable: 4996)
#include <iostream>
#include <memory>
#include <algorithm>
#include <chrono>
#include <cmath>
#include <amp.h>
#include <amp_math.h>
#pragma warning(pop)

class Timer
{
	typedef std::chrono::time_point<std::chrono::system_clock> time_point;

	time_point begin;

public:
	void Start()
	{
		this->begin = std::chrono::system_clock::now();
	}

	std::chrono::milliseconds Time()
	{
		const auto end = std::chrono::system_clock::now();
		return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
	}
};

struct Vector4
{
public:
	double data[4];

	Vector4()
	{
		data[0] = 0;
		data[1] = 0;
		data[2] = 0;
		data[3] = 0;
	}

	Vector4() restrict(amp)
	{
		data[0] = 0;
		data[1] = 0;
		data[2] = 0;
		data[3] = 0;
	}

	Vector4(const double x, const double y, const double z)
	{
		data[0] = x;
		data[1] = y;
		data[2] = z;
		data[3] = 0;
	}
};

// なにもしない
static void Normal(Vector4 x[], Vector4 v[], Vector4 f[], const double m, const double dt, const std::size_t n)
{
	const double tmp = dt*dt / 2;
	const double rm = 1.0 / m;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			f[i].data[j] = 0;
		}

		for (int j = 0; j < n; j++)
		{
			if (i != j)
			{
				Vector4 r;
				for (int k = 0; k < 4; k++)
				{
					r.data[k] = x[j].data[k] - x[i].data[k];
				}

				double r2 = 0;
				for (int k = 0; k < 4; k++)
				{
					r2 += r.data[k] * r.data[k];
				}
				const double r3 = r2 * std::sqrt(r2);

				for (int k = 0; k < 4; k++)
				{
					f[i].data[k] += r.data[k] / r3;
				}
			}
		}
	}

	for (int i = 0; i < n; i++)
	{
		// a = f/m
		Vector4 a;
		for (int j = 0; j < 4; j++)
		{
			a.data[j] = f[i].data[j] * rm;
		}

		// x += v*dt + a*dt*dt/2
		for (int j = 0; j < 4; j++)
		{
			const double dxv = v[i].data[j] * dt;
			const double dxa = a.data[j] * tmp;
			const double dx = dxv + dxa;
			x[i].data[j] += dx;
		}

		// v += a*dt
		for (int j = 0; j < 4; j++)
		{
			const double dv = a.data[j] * dt;
			v[i].data[j] += dv;
		}
	}
}

// C++ AMP
static void CppAmp(concurrency::array_view<Vector4>& x, concurrency::array_view<Vector4>& v, concurrency::array_view<Vector4>& f, const double m, const double dt, const std::size_t n)
{
	concurrency::parallel_for_each(
		x.get_extent(),
		[&f, x, n](const concurrency::index<1> idx) restrict(amp)
	{
		const int i = idx[0];

		for (int j = 0; j < 4; j++)
		{
			f[i].data[j] = 0;
		}

		for (int j = 0; j < n; j++)
		{
			if (i != j)
			{
				Vector4 r;
				for (int k = 0; k < 4; k++)
				{
					r.data[k] = x[j].data[k] - x[i].data[k];
				}

				double r2 = 0;
				for (int k = 0; k < 4; k++)
				{
					r2 += r.data[k] * r.data[k];
				}
				const double r3 = r2 * concurrency::precise_math::sqrt(r2);

				for (int k = 0; k < 4; k++)
				{
					f[i].data[k] += r.data[k] / r3;
				}
			}
		}
	});

	const double tmp = dt*dt / 2;
	const double rm = 1.0 / m;

	concurrency::parallel_for_each(
		x.get_extent(),
		[f, &x, &v, dt, tmp, rm](const concurrency::index<1> idx) restrict(amp)
	{
		const int i = idx[0];

		// a = f/m
		Vector4 a;
		for (int j = 0; j < 4; j++)
		{
			a.data[j] = f[i].data[j] * rm;
		}

		// x += v*dt + a*dt*dt/2
		for (int j = 0; j < 4; j++)
		{
			const double dxv = v[i].data[j] * dt;
			const double dxa = a.data[j] * tmp;
			const double dx = dxv + dxa;
			x[i].data[j] += dx;
		}

		// v += a*dt
		for (int j = 0; j < 4; j++)
		{
			const double dv = a.data[j] * dt;
			v[i].data[j] += dv;
		}
	});
}

int main()
{
	constexpr std::size_t n = 30000;
	constexpr int loop = 3;

	std::unique_ptr<Vector4[]> v(new Vector4[n]);
	std::unique_ptr<Vector4[]> x(new Vector4[n]);
	std::unique_ptr<Vector4[]> f(new Vector4[n]);
	auto generator = [](){return static_cast<double>(1 + std::rand()) / std::rand(); };
	auto generator4 = [generator](){return Vector4(generator(), generator(), generator()); };
	std::generate_n(v.get(), n, generator4);
	std::generate_n(x.get(), n, generator4);

	Timer timer;

	const double dt = 0.1;
	const double m = 2.5;

	// なにもしない
	std::unique_ptr<Vector4[]> vNormal(new Vector4[n]);
	std::unique_ptr<Vector4[]> xNormal(new Vector4[n]);
	{
		std::copy_n(v.get(), n, vNormal.get());
		std::copy_n(x.get(), n, xNormal.get());

		std::cout << "Normal:  ";
		timer.Start();
		for (int i = 0; i < loop; i++)
		{
			Normal(xNormal.get(), vNormal.get(), f.get(), m, dt, n);
		}
		const auto normalTime = timer.Time();
		std::cout << normalTime.count() << "[ms]" << std::endl;
	}

	// C++ AMP
	std::unique_ptr<Vector4[]> vCppAmp(new Vector4[n]);
	std::unique_ptr<Vector4[]> xCppAmp(new Vector4[n]);
	{
		std::copy_n(v.get(), n, vCppAmp.get());
		std::copy_n(x.get(), n, xCppAmp.get());

		// 一番最後のデバイスを既定デバイスに指定する
		{
			const auto devices = concurrency::accelerator::get_all();
			const auto device = std::find_if(devices.crbegin(), devices.crend(), [](const decltype(devices)::value_type& d)
			{
				return !(d.get_is_emulated());
			});

			if(device == devices.crend())
			{
				throw "Useful device not found";
			}

			concurrency::accelerator::set_default(device->get_device_path());
		}

		std::cout << "C++ AMP: ";
		timer.Start();

		// デバイス側メモリを作成
		concurrency::array_view<Vector4> vv(n, vCppAmp.get());
		concurrency::array_view<Vector4> xx(n, xCppAmp.get());
		concurrency::array_view<Vector4> ff(n, f.get());

		for (int i = 0; i < loop; i++)
		{
			CppAmp(xx, vv, ff, m, dt, n);
		}
		xx.synchronize();
		vv.synchronize();


		const auto time = timer.Time();
		std::cout << time.count() << "[ms]" << std::endl;
	}

	// エラーチェック
	const double eps = 1e-8;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const double errorV = std::abs(vNormal[i].data[j] - vCppAmp[i].data[j]) / vNormal[i].data[j];
			if (errorV > eps)
			{
				std::cout << "error V[" << i << "][" << j << "]: " << errorV << std::endl;
			}

			const double errorX = std::abs(xNormal[i].data[j] - xCppAmp[i].data[j]) / xNormal[i].data[j];
			if (errorV > eps)
			{
				std::cout << "error X[" << i << "][" << j << "]: " << errorX << std::endl;
			}
		}
	}

	return 0;
}
