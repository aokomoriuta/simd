#define _SCL_SECURE_NO_WARNINGS
#define __CL_ENABLE_EXCEPTIONS

#ifdef _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4996)
#include <intrin.h>
#endif
#ifdef __GCC
#include <avxintrin.h>
#endif
#include <iostream>
#include <memory>
#include <algorithm>
#include <chrono>
#include <CL/cl.hpp>
#ifdef _OPENMP
#include <omp.h>
#endif
#ifdef _MCS_VER
#pragma warning(pop)
#endif

#define OCL_EXTERNAL_INCLUDE(x) #x
const char srcStr[] =
#include "ocl.cl"
;

class Timer
{
	typedef std::chrono::time_point<std::chrono::system_clock> time_point;

	time_point begin;

public:
	void Start()
	{
		this->begin = std::chrono::system_clock::now();
	}

	std::chrono::milliseconds Time()
	{
		const auto end = std::chrono::system_clock::now();
		return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
	}
};

struct Vector4
{
public:
	double data[4];

	Vector4()
	{
		data[0] = 0;
		data[1] = 0;
		data[2] = 0;
		data[3] = 0;
	}

	Vector4(const double x, const double y, const double z)
	{
		data[0] = x;
		data[1] = y;
		data[2] = z;
		data[3] = 0;
	}
};

// なにもしない
static void Normal(Vector4 x[], Vector4 v[], Vector4 f[], const double m, const double dt, const std::size_t n)
{
	const double tmp = dt*dt / 2;
	const double rm = 1.0 / m;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			f[i].data[j] = 0;
		}

		for (int j = 0; j < n; j++)
		{
			if (i != j)
			{
				Vector4 r;
				for (int k = 0; k < 4; k++)
				{
					r.data[k] = x[j].data[k] - x[i].data[k];
				}

				double r2 = 0;
				for (int k = 0; k < 4; k++)
				{
					r2 += r.data[k] * r.data[k];
				}
				const double r3 = r2 * std::sqrt(r2);

				for (int k = 0; k < 4; k++)
				{
					f[i].data[k] += r.data[k] / r3;
				}
			}
		}
	}

	for (int i = 0; i < n; i++)
	{
		// a = f/m
		Vector4 a;
		for (int j = 0; j < 4; j++)
		{
			a.data[j] = f[i].data[j] * rm;
		}

		// x += v*dt + a*dt*dt/2
		for (int j = 0; j < 4; j++)
		{
			const double dxv = v[i].data[j] * dt;
			const double dxa = a.data[j] * tmp;
			const double dx = dxv + dxa;
			x[i].data[j] += dx;
		}

		// v += a*dt
		for (int j = 0; j < 4; j++)
		{
			const double dv = a.data[j] * dt;
			v[i].data[j] += dv;
		}
	}
}

#ifdef _OPENMP
// なにもしない+OpenMP
static void NormalOmp(Vector4 x[], Vector4 v[], Vector4 f[], const double m, const double dt, const std::size_t n)
{
	const double tmp = dt*dt / 2;
	const double rm = 1.0 / m;

	#pragma omp parallel
	{
		#pragma omp for
		for (int i = 0; i < n; i++)
		{
			// f = Σ(x_j - x)/|x_j - x|^3
			for (int j = 0; j < 4; j++)
			{
				f[i].data[j] = 0;
			}

			for (int j = 0; j < n; j++)
			{
				if (i != j)
				{
					Vector4 r;
					for (int k = 0; k < 4; k++)
					{
						r.data[k] = x[j].data[k] - x[i].data[k];
					}

					double r2 = 0;
					for (int k = 0; k < 4; k++)
					{
						r2 += r.data[k] * r.data[k];
					}
					const double r3 = r2 * std::sqrt(r2);

					for (int k = 0; k < 4; k++)
					{
						f[i].data[k] += r.data[k] / r3;
					}
				}
			}
		}

		#pragma omp for
		for (int i = 0; i < n; i++)
		{
			// a = f/m
			Vector4 a;
			for (int j = 0; j < 4; j++)
			{
				a.data[j] = f[i].data[j] * rm;
			}

			// x += v*dt + a*dt*dt/2
			for (int j = 0; j < 4; j++)
			{
				const double dxv = v[i].data[j] * dt;
				const double dxa = a.data[j] * tmp;
				const double dx = dxv + dxa;
				x[i].data[j] += dx;
			}

			// v += a*dt
			for (int j = 0; j < 4; j++)
			{
				const double dv = a.data[j] * dt;
				v[i].data[j] += dv;
			}
		}
	}
}
#endif

static inline __m256d dot(const __m256d a, const __m256d b)
{
	const __m256d ab = _mm256_mul_pd(a, b);

	const __m256d sum0 = _mm256_hadd_pd(ab, ab);
	const __m256d sum1 = _mm256_permute2f128_pd(sum0, sum0, 1);
	const __m256d sum2 = _mm256_unpacklo_pd(sum0, sum1);
	const __m256d d = _mm256_hadd_pd(sum2, sum2);

	return d;
}

#ifdef __AVX2__
static inline __m256d fma(const __m256d a, const __m256d b, const __m256d c)
{
	return _mm256_fmadd_pd(a, b, xc)
}
#else
static inline __m256d fma(const __m256d a, const __m256d b, const __m256d c)
{
	const __m256d m = _mm256_mul_pd(a, b);
	return _mm256_add_pd(m, c);
}
#endif

#ifdef __clang__
using Vector4Aligned = Vector4 __attribute((aligned(32)));
#else
using Vector4Aligned alignas(32) = Vector4;
#endif

// SIMD(AVX)
static void Simd(Vector4 x[], Vector4 v[], Vector4 f[], const double m, const double dt, const std::size_t n)
{
	const __m256d dt2 = _mm256_broadcast_sd(&dt);
	const double tmp = dt*dt / 2;
	const __m256d tmp2 = _mm256_broadcast_sd(&tmp);
	const double rm = 1.0 / m;
	const __m256d rm2 = _mm256_broadcast_sd(&rm);

	for (int i = 0; i < n; i++)
	{
		const Vector4Aligned xi(x[i]);
		const __m256d x2 = _mm256_load_pd(xi.data);

		// f = Σ(x_j - x)/|x_j - x|^3
		__m256d f2 = _mm256_setzero_pd();
		for (int j = 0; j < n; j++)
		{
			if (i != j)
			{
				const Vector4Aligned xj(x[j]);
				const __m256d xj2 = _mm256_load_pd(xj.data);
				const __m256d r = _mm256_sub_pd(xj2, x2);

				const __m256d r2 = dot(r, r);
				const __m256d r1 = _mm256_sqrt_pd(r2);
				const __m256d r3 = _mm256_mul_pd(r1, r2);

				const __m256d df = _mm256_div_pd(x2, r3);
				f2 = _mm256_add_pd(f2, df);
			}
		}

		Vector4Aligned fi(f[i]);
		_mm256_store_pd(fi.data, f2);
		f[i] = fi;
	}

	for (int i = 0; i < n; i++)
	{
		// a = f/m
		const Vector4Aligned fi(f[i]);
		const __m256d f2 = _mm256_load_pd(fi.data);
		const __m256d a = _mm256_mul_pd(f2, rm2);

		// x += v*dt + a*dt*dt/2
		Vector4Aligned xi(x[i]);
		Vector4Aligned vi(v[i]);
		const __m256d x2 = _mm256_load_pd(xi.data);
		const __m256d v2 = _mm256_load_pd(vi.data);
		const __m256d x3 = fma(v2, dt2, x2);
		const __m256d x4 = fma(a, tmp2, x3);
		_mm256_store_pd(xi.data, x4);
		x[i] = xi;

		// v += a*dt
		const __m256d v3 = fma(a, dt2, v2);
		_mm256_store_pd(vi.data, v3);
		v[i] = vi;
	}
}

#ifdef _OPENMP
// SIMD(AVX) + OpenMP
static void SimdOmp(Vector4 x[], Vector4 v[], Vector4 f[], const double m, const double dt, const std::size_t n)
{
	const __m256d dt2 = _mm256_broadcast_sd(&dt);
	const double tmp = dt*dt / 2;
	const __m256d tmp2 = _mm256_broadcast_sd(&tmp);
	const double rm = 1.0 / m;
	const __m256d rm2 = _mm256_broadcast_sd(&rm);

	#pragma omp parallel
	{
		#pragma omp for
		for (int i = 0; i < n; i++)
		{
			const Vector4Aligned xi(x[i]);
			const __m256d x2 = _mm256_load_pd(xi.data);

			// f = Σ(x_j - x)/|x_j - x|^3
			__m256d f2 = _mm256_setzero_pd();
			for (int j = 0; j < n; j++)
			{
				if (i != j)
				{
					const Vector4Aligned xj(x[j]);
					const __m256d xj2 = _mm256_load_pd(xj.data);
					const __m256d r = _mm256_sub_pd(xj2, x2);

					const __m256d r2 = dot(r, r);
					const __m256d r1 = _mm256_sqrt_pd(r2);
					const __m256d r3 = _mm256_mul_pd(r1, r2);

					const __m256d df = _mm256_div_pd(x2, r3);
					f2 = _mm256_add_pd(f2, df);
				}
			}

			Vector4Aligned fi(f[i]);
			_mm256_store_pd(fi.data, f2);
			f[i] = fi;
		}

		#pragma omp for
		for (int i = 0; i < n; i++)
		{
			// a = f/m
			const Vector4Aligned fi(f[i]);
			const __m256d f2 = _mm256_load_pd(fi.data);
			const __m256d a = _mm256_mul_pd(f2, rm2);

			// x += v*dt + a*dt*dt/2
			Vector4Aligned xi(x[i]);
			Vector4Aligned vi(v[i]);
			const __m256d x2 = _mm256_load_pd(xi.data);
			const __m256d v2 = _mm256_load_pd(vi.data);
			const __m256d x3 = fma(v2, dt2, x2);
			const __m256d x4 = fma(a, tmp2, x3);
			_mm256_store_pd(xi.data, x4);
			x[i] = xi;

			// v += a*dt
			const __m256d v3 = fma(a, dt2, v2);
			_mm256_store_pd(vi.data, v3);
			v[i] = vi;
		}
	}
}
#endif

int main()
{
	const std::size_t n = 30000;
	const int loop = 3;

	std::unique_ptr<Vector4[]> v(new Vector4[n]);
	std::unique_ptr<Vector4[]> x(new Vector4[n]);
	std::unique_ptr<Vector4[]> f(new Vector4[n]);
	auto generator = [](){return static_cast<double>(1 + std::rand()) / std::rand(); };
	auto generator4 = [generator](){return Vector4(generator(), generator(), generator()); };
	std::generate_n(v.get(), n, generator4);
	std::generate_n(x.get(), n, generator4);

	Timer timer;

	const double dt = 0.1;
	const double m = 2.5;

	// なにもしない
	std::unique_ptr<Vector4[]> vNormal(new Vector4[n]);
	std::unique_ptr<Vector4[]> xNormal(new Vector4[n]);
	{
		std::copy_n(v.get(), n, vNormal.get());
		std::copy_n(x.get(), n, xNormal.get());

		std::cout << "Normal: ";
		timer.Start();
		for (int i = 0; i < loop; i++)
		{
			Normal(xNormal.get(), vNormal.get(), f.get(), m, dt, n);
		}
		const auto normalTime = timer.Time();
		std::cout << normalTime.count() << "[ms]" << std::endl;
	}

	// SIMD
	std::unique_ptr<Vector4[]> vSimd(new Vector4[n]);
	std::unique_ptr<Vector4[]> xSimd(new Vector4[n]);
	{
		std::copy_n(v.get(), n, vSimd.get());
		std::copy_n(x.get(), n, xSimd.get());

		std::cout << "Simd  : ";
		timer.Start();
		for (int i = 0; i < loop; i++)
		{
			Simd(xSimd.get(), vSimd.get(), f.get(), m, dt, n);
		}
		const auto normalTime = timer.Time();
		std::cout << normalTime.count() << "[ms]" << std::endl;
	}

#ifdef _OPENMP
#pragma omp parallel
#pragma omp master
	{
		std::cout << "OpenMP: " << omp_get_num_threads() << "threads" << std::endl;
	}

	// なにもしない+OponMP
	std::unique_ptr<Vector4[]> vNormalOmp(new Vector4[n]);
	std::unique_ptr<Vector4[]> xNormalOmp(new Vector4[n]);
	{
		std::copy_n(v.get(), n, vNormalOmp.get());
		std::copy_n(x.get(), n, xNormalOmp.get());

		std::cout << "Normal: ";
		timer.Start();
		for (int i = 0; i < loop; i++)
		{
			NormalOmp(xNormalOmp.get(), vNormalOmp.get(), f.get(), m, dt, n);
		}
		const auto normalOmpTime = timer.Time();
		std::cout << normalOmpTime.count() << "[ms]" << std::endl;
	}

	// SIMD+OpenMP
	std::unique_ptr<Vector4[]> vSimdOmp(new Vector4[n]);
	std::unique_ptr<Vector4[]> xSimdOmp(new Vector4[n]);
	{
		std::copy_n(v.get(), n, vSimdOmp.get());
		std::copy_n(x.get(), n, xSimdOmp.get());

		std::cout << "Simd  : ";
		timer.Start();
		for (int i = 0; i < loop; i++)
		{
			SimdOmp(xSimdOmp.get(), vSimdOmp.get(), f.get(), m, dt, n);
		}
		const auto normalTime = timer.Time();
		std::cout << normalTime.count() << "[ms]" << std::endl;
	}
#endif

	// OpenCL
	std::unique_ptr<Vector4[]> vOcl(new Vector4[n]);
	std::unique_ptr<Vector4[]> xOcl(new Vector4[n]);
	{
		std::copy_n(v.get(), n, vOcl.get());
		std::copy_n(x.get(), n, xOcl.get());

		std::cout << "OpenCL: ";

		// プラットフォーム取得（複数ある場合は一番最後）
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		const auto& platform = *(platforms.rbegin());

		// デバイスを取得（複数ある場合は一番最後）
		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
		const auto& device = *(devices.rbegin());

		// コンテキスト作成
		const cl::Context context(device);

		// プログラムの作成＆ビルド
		cl::Program::Sources sources;
		sources.push_back(std::make_pair(srcStr, std::strlen(srcStr)));
		cl::Program program(context, sources);
		try
		{
			program.build(devices);
		}
		// OpenCL例外があった場合
		catch (cl::Error error)
		{
			// ビルドエラーなら
			if (error.err() == CL_BUILD_PROGRAM_FAILURE)
			{
				// ビルドログを表示
				std::cout << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device);
			}
			else
			{
				std::cout << "Unknown error #" << error.err() << " @ " << error.what() << std::endl;
			}
			return -1;
		}

		// カーネルを作成
		const std::string KERNEL_FUNCTION_NAME_FORCE = "force";
		const std::string KERNEL_FUNCTION_NAME_MOVE = "move";
		cl::Kernel kernelForce(program, KERNEL_FUNCTION_NAME_FORCE.c_str());
		cl::Kernel kernelMove(program, KERNEL_FUNCTION_NAME_MOVE.c_str());

		const std::size_t size = sizeof(Vector4) * n;
		cl::Buffer bufferX(context, CL_MEM_READ_WRITE, size);
		cl::Buffer bufferV(context, CL_MEM_READ_WRITE, size);
		cl::Buffer bufferF(context, CL_MEM_READ_WRITE, size);

		kernelForce.setArg(0, bufferF);
		kernelForce.setArg(1, bufferX);
		kernelForce.setArg(2, static_cast<cl_ulong>(n));

		kernelMove.setArg(0, bufferX);
		kernelMove.setArg(1, bufferV);
		kernelMove.setArg(2, bufferF);
		kernelMove.setArg(3, static_cast<cl_double>(m));
		kernelMove.setArg(4, static_cast<cl_double>(dt));

		// キュー作成
		const cl::CommandQueue queue(context, device);

		timer.Start();

		// ホスト->デバイス
		queue.enqueueWriteBuffer(bufferX, CL_FALSE, 0, size, xOcl.get());
		queue.enqueueWriteBuffer(bufferV, CL_FALSE, 0, size, vOcl.get());

		for (int i = 0; i < loop; i++)
		{
			// 実行
			cl::Event kernelEvent;
			queue.enqueueNDRangeKernel(kernelForce, cl::NullRange, cl::NDRange(n), cl::NullRange, NULL, &kernelEvent);
			queue.enqueueNDRangeKernel(kernelMove, cl::NullRange, cl::NDRange(n), cl::NullRange, NULL, &kernelEvent);
			kernelEvent.wait();
		}

		// デバイス->ホスト
		queue.enqueueReadBuffer(bufferX, CL_TRUE, 0, size, xOcl.get());
		queue.enqueueReadBuffer(bufferV, CL_TRUE, 0, size, vOcl.get());

		const auto oclTime = timer.Time();
		std::cout << oclTime.count() << "[ms]" << std::endl;

		std::cout << "== Platform : " << platform() << " ==" << std::endl;
		std::cout <<
			"Name    : " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl <<
			"Vendor  : " << platform.getInfo<CL_PLATFORM_VENDOR>() << std::endl <<
			"Version : " << platform.getInfo<CL_PLATFORM_VERSION>() << std::endl;

		std::cout << "== Device : " << device() << " ==" << std::endl;
		std::cout <<
			"Name                                             : " << device.getInfo<CL_DEVICE_NAME>() << std::endl <<
			"Vendor                                           : " << device.getInfo<CL_DEVICE_VENDOR>() << " (ID:" << device.getInfo<CL_DEVICE_VENDOR_ID>() << ")" << std::endl <<
			"Version                                          : " << device.getInfo<CL_DEVICE_VERSION>() << std::endl <<
			"Driver version                                   : " << device.getInfo<CL_DRIVER_VERSION>() << std::endl <<
			"OpenCL C version                                 : " << device.getInfo<CL_DEVICE_OPENCL_C_VERSION>() << std::endl;
	}

#if 0
	// エラーチェック
	const double eps = 1e-8;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const double errorV = std::abs(vNormal[i].data[j] - vSimd[i].data[j]) / vNormal[i].data[j];
			if (errorV > eps)
			{
				std::cout << "error Simd V[" << i << "][" << j << "]: " << errorV << std::endl;
			}

			const double errorX = std::abs(xNormal[i].data[j] - xSimd[i].data[j]) / xNormal[i].data[j];
			if (errorV > eps)
			{
				std::cout << "error Simd X[" << i << "][" << j << "]" << errorX << std::endl;
			}
		}
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			const double errorV = std::abs(vNormal[i].data[j] - vOcl[i].data[j]) / vNormal[i].data[j];
			if (errorV > eps)
			{
				std::cout << "error OpenCL V[" << i << "][" << j << "]: " << errorV << std::endl;
			}

			const double errorX = std::abs(xNormal[i].data[j] - xOcl[i].data[j]) / xNormal[i].data[j];
			if (errorV > eps)
			{
				std::cout << "error OpenCL X[" << i << "][" << j << "]" << errorX << std::endl;
			}
		}
	}
#endif

	return 0;
}

